#!/bin/sh
# Script for acme cleanup after DNS validation on hurricane electrics.

# CERTBOT_DOMAIN: The domain being authenticated
# CERTBOT_VALIDATION: The validation string
# CERTBOT_TOKEN: Resource name part of the HTTP-01 challenge (HTTP-01 only)
# CERTBOT_REMAINING_CHALLENGES: Number of challenges remaining after the current challenge
# CERTBOT_ALL_DOMAINS: A comma-separated list of all domains challenged for the current certificate
# Additionally for cleanup:
# CERTBOT_AUTH_OUTPUT: Whatever the auth script wrote to stdout

echo "Reset ACME DNS token for ${CERTBOT_DOMAIN}"
wget --spider -q --post-data "hostname=_acme-challenge.${CERTBOT_DOMAIN}&password=${DNS_ACCESS_TOKEN}&txt=IDLE" "https://dyn.dns.he.net/nic/update"