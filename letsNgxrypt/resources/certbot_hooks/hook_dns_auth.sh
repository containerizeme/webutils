#!/bin/sh
# Script to set acme challenge on a pre-configured TXT entry on hurricane electrics.
# Input arguments are provided through environment variables.
#
# DNS_ACCESS_TOKEN: The scrpts uses the pre-configured access token from hurricane electrics to access the TXT entry
#
# CERTBOT_DOMAIN: The domain being authenticated
# CERTBOT_VALIDATION: The validation string
# CERTBOT_TOKEN: Resource name part of the HTTP-01 challenge (HTTP-01 only)
# CERTBOT_REMAINING_CHALLENGES: Number of challenges remaining after the current challenge
# CERTBOT_ALL_DOMAINS: A comma-separated list of all domains challenged for the current certificate


echo "Set ACME DNS token for ${CERTBOT_DOMAIN}"
wget --spider -q --post-data "hostname=_acme-challenge.${CERTBOT_DOMAIN}&password=${DNS_ACCESS_TOKEN}&txt=${CERTBOT_VALIDATION}" "https://dyn.dns.he.net/nic/update"

# Sleep to let propagate over DNS
sleep 30