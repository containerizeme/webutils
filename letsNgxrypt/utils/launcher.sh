#!/bin/sh

if ! [[ -z "${REPO_URL}" ]]; then
  sh ${GIT_HELPERS_DIR}/setupRepoAccess.sh
fi

# If cached domains do NOT match the the configured domains
# run certbot
CACHED_DOMAINS=$(cat ${CBOTDOMAIN_CACHE_PATH})
if [ "${CACHED_DOMAINS}" != "${CBOT_DOMAINS}" ]; then
  echo "Run due to new domain configuration"
  certbot certonly ${CBOT_ARGS}

  # Cache certbot domains
  echo ${CBOT_DOMAINS} > ${CBOTDOMAIN_CACHE_PATH}
fi

# If not certificates are available, initially trigger certbot
if [ -z "$(ls -A ${CBOT_CERTIFICATES_DIR})" ]; then
  echo "Run due to missing certificates"
  certbot certonly ${CBOT_ARGS}

  # Cache certbot domains
  echo ${CBOT_DOMAINS} > ${CBOTDOMAIN_CACHE_PATH}
fi

# Start nginx, when the certificates are ready
nginx -g "daemon off;"&

# Execute the script periodically
# Renew certificates if needed
# Reload nginx to load new certificates
while [ true ]
do
  sleep ${CBOT_EXECUTION_PERIOD}
  
  echo "Renew certificates if needed"
  certbot renew --deploy-hook "nginx -s reload"
done
